﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WelcomeEvent
{
    public string status;
    public string session;
    public bool is_admin;
}

public class LobbyListEvent
{
    public class Item
    {
        public string name;
        public int point;
        public double reliability;
        public string status;
    }

    public List<Item> list = new List<Item>();
}

public class MatchStartEvent
{
    public class Item
    {
        public string name;
        public int point;
        public double reliability;
    }

    public Item foe;
    public int template_id;
}


public class TimeToDeclareEvent
{
    public double rest_time;
    public bool pass_available;
}

public class FoeDeclareEvent
{
    public string what;
}

public class TimeToChooseEvent
{
    public double rest_time;
}

public class MatchResultEvent
{
    public string foe_choice;
}

public class TimerSetEvent
{
    public int time;
}

public class GameOverEvent
{
}

public class YourDeclareEvent
{
    public string your_declare;
}

public class YourChoiceEvent
{
    public string your_choice;
}

public class ChatEvent
{
    public string name;
    public string msg;
}