﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class RestTimeTxt : MonoBehaviour
{
    Text txt;

    DateTime overTime;
    bool clocking = false;
    int cachedN;

    void Awake()
    {
        txt = GetComponent<Text>();
    }

    public void Set(int restTimeInSec)
    {
        overTime = DateTime.Now.AddSeconds(restTimeInSec);
        clocking = true;
    }

    void Update()
    {
        if (!clocking)
        {
            return;
        }

        int restTimeInSec = (int)((overTime - DateTime.Now).TotalSeconds);
        if (restTimeInSec < 0)
        {
            return;
        }

        if (restTimeInSec != cachedN)
        {
            txt.text = restTimeInSec.ToString() + "초";
            cachedN = restTimeInSec;
        }
    }
}

