﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChatItem : MonoBehaviour
{
    public Text playerTxt;
    public Text chatTxt;

    public void Apply(Player player, string msg)
    {
        playerTxt.text = player.name;
        playerTxt.color = player.color;

        chatTxt.text = " : " + msg;
    }
}
