﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LobbyUserItem : MonoBehaviour
{
    public Text lhs;
    public Text nameTxt;
    public Text rhs;
    public Text statusTxt;

    Player player;

    public void Apply(Player player)
    {
        this.player = player;

        player.onChanged += UpdateInfo;
        UpdateInfo();
    }

    void UpdateInfo()
    {
        lhs.text = string.Format("{0}. ", player.rank);

        nameTxt.text = player.name;
        nameTxt.color = player.color;

        var reliability = player.reliability < 0 ? "?" : Mathf.FloorToInt(100 * player.reliability).ToString();
        rhs.text = string.Format("{0}점 신뢰도 {1}%", player.point, reliability);

        if (player.status == Player.Status.Lobby)
        {
            statusTxt.text = string.Format("대기중");
            statusTxt.color = Color.blue;
        }
        else
        {
            statusTxt.text = string.Format("게임중");
            statusTxt.color = Color.red;
        }
    }
}
