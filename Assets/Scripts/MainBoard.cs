﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainBoard : MonoBehaviour
{
    public GameObject group;
    public Text whenFoeConfessTxt;
    public Text whenFoeSilentTxt;
    public Text whenFoeConfessFoePointTxt;
    public Text whenFoeConfessMyPointTxt;
    public Text whenFoeSilentFoePointTxt;
    public Text whenFoeSilentMyPointTxt;

    public GameObject tableGroup;
    public Text _0_0_lhs;
    public Text _0_0_rhs;
    public Text _1_0_lhs;
    public Text _1_0_rhs;
    public Text _0_1_lhs;
    public Text _0_1_rhs;
    public Text _1_1_lhs;
    public Text _1_1_rhs;

    public void ShowTable(MatchTemplate template)
    {
        group.SetActive(false);

        _0_0_lhs.text = _0_0_rhs.text = template.all_confess.ToString();
        _0_1_lhs.text = _1_0_rhs.text = template.one_confess.ToString();
        _0_1_rhs.text = _1_0_lhs.text = template.one_silent.ToString();
        _1_1_lhs.text = _1_1_rhs.text = template.all_silent.ToString();

        tableGroup.gameObject.SetActive(true);
    }

    public void ShowCases(string myChoice, string myDeclare, string foeDeclare, MatchTemplate template)
    {
        tableGroup.gameObject.SetActive(false);

        When(myChoice, myDeclare, foeDeclare, template
            , "confess", whenFoeConfessTxt, whenFoeConfessMyPointTxt, whenFoeConfessFoePointTxt);

        When(myChoice, myDeclare, foeDeclare, template
            , "silent", whenFoeSilentTxt, whenFoeSilentMyPointTxt, whenFoeSilentFoePointTxt);

        group.SetActive(true);
    }

    void When(string myChoice, string myDeclare, string foeDeclare, MatchTemplate template, string when
        , Text desc, Text myPointTxt, Text foePointTxt)
    {
        bool myBetrayed = (myDeclare != null && myDeclare != "pass" && myDeclare != myChoice);
        bool myTrusted = (myDeclare != null && myDeclare == myChoice);

        bool foeBetrayed = (foeDeclare != null && foeDeclare != "pass" && foeDeclare != when);
        bool foeTrusted = (foeDeclare != null && foeDeclare == when);

        desc.text = GetDescTxt(myBetrayed, foeBetrayed, myTrusted, foeTrusted);
        var myPoint = template.GetPoint(myChoice, when);
        var foePoint = template.GetPoint(when, myChoice);
        myPointTxt.text = GetPointTxt(myPoint);
        foePointTxt.text = GetPointTxt(foePoint);
    }

    string GetDescTxt(bool myBetrayed, bool foeBetrayed
        , bool myTrusted, bool foeTrusted)
    {
        if (myBetrayed && foeBetrayed)
        {
            return "서로의 배신";
        }
        else if (myBetrayed && !foeBetrayed)
        {
            return "나의 배신";
        }
        else if (!myBetrayed && foeBetrayed)
        {
            return "상대의 배신";
        }
        else if (myTrusted && foeTrusted)
        {
            return "신뢰";
        }
        else
        {
            return "";
        }
    }

    string GetPointTxt(int point)
    {
        var pointTxt = point.ToString();
        if (point >= 0)
        {
            pointTxt = "+" + pointTxt;
        }
        return pointTxt;
    }
}
