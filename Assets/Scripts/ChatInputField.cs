﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChatInputField : MonoBehaviour
{
    public RectTransform sock;
    public ChatItem chatItemPrefab;

    InputField input;

    void Awake()
    {
        input = GetComponent<InputField>();
        input.onEndEdit.AddListener(OnEndEdit);
    }

    public void OnChatMessage(Player player, string msg)
    {
        if (!gameObject.activeInHierarchy)
        {
            return;
        }

        var chatItem = Instantiate(chatItemPrefab);

        chatItem.Apply(player, msg);

        chatItem.GetComponent<RectTransform>().SetParent(sock, false);
        //        chatItem.transform.SetParent(sock);
        //        chatItem.transform.SetAsLastSibling();

    }

    void OnEndEdit(string str)
    {
        if (str.Length == 0)
        {
            return;
        }

        OnChatMessage(Game.GetInstance().me, str);
        input.text = "";
        input.ActivateInputField();
        input.Select();
        Game.GetInstance().Chat(str);
//        EventSystem.current.SetSelectedGameObject(input.gameObject, null);
//        input.OnPointerClick(null);
    }

}
