﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleScene : MonoBehaviour
{
    const string NAME_KEY = "NAME";

    public InputField nameInput;

	void OnEnable ()
    {
        nameInput.text = PlayerPrefs.GetString(NAME_KEY, "");
        SwitchAni.GetInstance().Run();
    }
	
	void OnDisable ()
    {
        PlayerPrefs.SetString(NAME_KEY, nameInput.text);
    }
}
