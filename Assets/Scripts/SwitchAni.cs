﻿using UnityEngine;
using System.Collections;
using Kino;

public class SwitchAni : MonoBehaviour
{
    public AnimationCurve scanLineJitter;
    public AnimationCurve verticalJump;
    public AnimationCurve horizontalShake;
    public AnimationCurve colorDrift;

    public float dura;

    static SwitchAni instance;
    AnalogGlitch glitch;

    void Awake()
    {
        glitch = GetComponent<AnalogGlitch>();
        instance = this;
    }

    public void Run()
    {
        StartCoroutine(Go());
    }

    public static SwitchAni GetInstance()
    {
        return instance;
    }

    IEnumerator Go()
    {
        var accumTime = 0f;

        glitch.enabled = true;

        while (accumTime < dura)
        {
            var normalizedTime = accumTime / dura;
            glitch.scanLineJitter = scanLineJitter.Evaluate(normalizedTime);
            glitch.verticalJump = verticalJump.Evaluate(normalizedTime);
            glitch.horizontalShake = horizontalShake.Evaluate(normalizedTime);
            glitch.colorDrift = colorDrift.Evaluate(normalizedTime);

            yield return null;
            accumTime += Time.deltaTime;
        }

        glitch.enabled = false;
    }
}
