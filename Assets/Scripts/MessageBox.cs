﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    public Text txt;
    public GameObject obj;

    static MessageBox instance;

    public static void Show(string msg)
    {
        instance._Show(msg);
    }

    public static void Hide()
    {
        instance._Hide();
    }

    public static bool IsShown()
    {
        return instance.obj.activeSelf;
    }

    public void _Show(string msg)
    {
        txt.text = msg;
        obj.SetActive(true);
    }

    public void _Hide()
    {
        obj.SetActive(false);
    }

    void Awake()
    {
        instance = this;
    }

    void OnDestroy()
    {
        instance = null;
    }
}
