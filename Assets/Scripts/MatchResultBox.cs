﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MatchResultBox : MonoBehaviour
{
    public Text betrayedTxt;
    public Text trustTxt;

    public Image BGImg;
    public Sprite betrayedBG;
    public Sprite trustBG;

    public Text myNameTxt;
    public Text foeNameTxt;

    public Text myChoiceTxt;
    public Text foeChoiceTxt;

    public delegate void OnDisableAction();
    public event OnDisableAction onDisabled;

    public void Apply(Player me, Player foe, string myDeclare, string foeDeclare, string myChoice, string foeChoice
        , MatchTemplate template)
    {
        if (myDeclare != myChoice || foeDeclare != foeChoice)
        {
            betrayedTxt.gameObject.SetActive(true);
            trustTxt.gameObject.SetActive(false);
            BGImg.sprite = betrayedBG;
        }
        else
        {
            betrayedTxt.gameObject.SetActive(false);
            trustTxt.gameObject.SetActive(true);
            BGImg.sprite = trustBG;
        }

        myNameTxt.text = me.name;
        myNameTxt.color = me.color;
        foeNameTxt.text = foe.name;
        foeNameTxt.color = foe.color;

        int myPoint = template.GetPoint(myChoice, foeChoice);
        int foePoint = template.GetPoint(foeChoice, myChoice);

        SetChoiceTxt(myChoice, myPoint, myChoiceTxt);
        SetChoiceTxt(foeChoice, foePoint, foeChoiceTxt);
    }

    void SetChoiceTxt(string choice, int point, Text txt)
    {
        var word = "";
        if (choice == "silent")
        {
            word = "침묵";
        }
        else if (choice == "confess")
        {
            word = "자백";
        }
        else
        {
            throw new System.Exception("Unhandled choice " + choice);
        }

        var pointTxt = point.ToString();
        if (point >= 0)
        {
            pointTxt = "+" + pointTxt;
        }

        txt.text = string.Format("{0}({1})", word, pointTxt);
    }

    void OnDisable()
    {
        if (onDisabled != null)
        {
            onDisabled();
        }
    }
}
