﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FoeDeclareBox : MonoBehaviour
{
    public Image questionImg;
    public Image confessImg;
    public Image silentImg;
    public Image passImg;

    public void Apply(string what)
    {
        DisableAll();

        if (what == "confess")
        {
            confessImg.gameObject.SetActive(true);
        }
        else if (what == "silent")
        {
            silentImg.gameObject.SetActive(true);
        }
        else if (what == "pass")
        {
            passImg.gameObject.SetActive(true);
        }
        else
        {
            questionImg.gameObject.SetActive(true);
        }
    }

    void OnEnable()
    {
        Apply("nothing");
    }

    void DisableAll()
    {
        questionImg.gameObject.SetActive(false);
        confessImg.gameObject.SetActive(false);
        silentImg.gameObject.SetActive(false);
        passImg.gameObject.SetActive(false);
    }
}
