﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerProfile : MonoBehaviour
{
    public Text nameTxt;
    public Text centerTxt;
    public Text reliabilityTxt;

    Player player = null;

    public void Apply(Player player)
    {
        this.player = player;

        if (player != null)
        {
            player.onChanged -= UpdateInfo;
        }

        player.onChanged += UpdateInfo;
        UpdateInfo();
    }

    void UpdateInfo()
    {
        nameTxt.text = player.name;
        nameTxt.color = player.color;
        centerTxt.text = string.Format("점수 {0}점({1}등)"
            , player.point, player.rank);
        var reliability = player.reliability < 0 ? "?" : Mathf.FloorToInt(100 * player.reliability).ToString();
        reliabilityTxt.text = string.Format("신뢰도 {0}%",
            reliability);
    }
}
