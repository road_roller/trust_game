﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;
using LitJson;

public class Game : MonoBehaviour
{
    public const string server_addr = "https://trustgame.herokuapp.com";
//    public const string server_addr = "http://127.0.0.1:8080";
    public InputField nameInputField;
    public MyInfo myInfo = new MyInfo();
    public string session;

    public SwitchAni switchAni;

    public RectTransform titleScene;
    public RectTransform lobbyScene;
    public RectTransform matchScene;

    public LobbyPlayerList lobbyPlayerList;
    public RectTransform adminToolGroup;
    public InputField gameOverTimeInputField;
    public MatchSearchPopup matchSearchPopup;
    public GameOverTimer gameOverTimer;
    public RectTransform playBtn;
    public ChatInputField chatInputField;

    public PlayerProfile myProfile;
    public PlayerProfile foeProfile;
    public FoeDeclareBox foeDeclareBox;
    public Image qq;
    public Button confessButton;
    public Button silentButton;
    public Button passButton;
    public Button declareButton;
    public Button chooseButton;
    public MatchResultBox matchResultBox;
    public Sprite qqSprite;
    public MainBoard mainBoard;
    public MyDeclareTxt myDeclareTxt;
    public Text restTimeLabel;
    public RestTimeTxt restTimeTxt;

    static Game instance;
//    Player.Status status = Player.Status.Lobby;
    public bool is_admin = false;
    List<Player> players = new List<Player>();
    Player foe;

    readonly static MatchTemplate [] matchTemplates = new MatchTemplate[] {
        new MatchTemplate(3, 1, 0, 6) // 죄수의 딜레마
	    , new MatchTemplate(1, -3, 0, 6) // 치킨 게임
	    , new MatchTemplate(3, 1, -3, 2) // 사슴 사냥
    };

    public static Game GetInstance()
    {
        return instance;
    }

    MatchTemplate template;
    int pollFailCount = 0;

    string myDeclare = null;
    string foeDeclare = null;
    string myChoice = null;
    string foeChoice = null;
    bool isChooseTime = false;

    public void ConnectToServer()
    {
        StartCoroutine(BeginConnectToServer());
    }

    public void SendPlayRequest()
    {
        StartCoroutine(BeginSendPlayRequest());
    }

    public void SendStayRequest()
    {
        matchSearchPopup.gameObject.SetActive(false);
        StartCoroutine(BeginSendStayRequest());
    }

    public void Declare()
    {
        StartCoroutine(BeginDeclare(myDeclare));

        declareButton.gameObject.SetActive(false);

        qq.sprite = qqSprite;
        mainBoard.ShowTable(template);

        restTimeLabel.gameObject.SetActive(false);
        restTimeTxt.gameObject.SetActive(false);
    }

    public void Choose()
    {
        StartCoroutine(BeginChoose(myChoice));

        chooseButton.gameObject.SetActive(false);
    }

    public void Cancel()
    {
        if (isChooseTime)
        {
            myChoice = null;
            chooseButton.interactable = false;
        }
        else
        {
            myDeclare = null;
            declareButton.interactable = false;
        }
        qq.sprite = qqSprite;
        mainBoard.ShowTable(template);
    }

    public void Pass()
    {
        if (!isChooseTime)
        {
            myDeclare = "pass";
            qq.sprite = passButton.image.sprite;
            mainBoard.ShowTable(template);
            declareButton.interactable = true;
        }
    }

    public void Confess()
    {
        if (isChooseTime)
        {
            myChoice = "confess";
            chooseButton.interactable = true;
        }
        else
        {
            myDeclare = "confess";
            declareButton.interactable = true;
        }
        qq.sprite = confessButton.image.sprite;
        mainBoard.ShowCases("confess", myDeclare, foeDeclare, template);
    }

    public void Silent()
    {
        if (isChooseTime)
        {
            myChoice = "silent";
            chooseButton.interactable = true;
        }
        else
        {
            myDeclare = "silent";
            declareButton.interactable = true;
        }
        qq.sprite = silentButton.image.sprite;
        mainBoard.ShowCases("silent", myDeclare, foeDeclare, template);
    }

    public void SetGameOverTimer()
    {
        StartCoroutine(BeginSetGameOverTimer());
    }

    string GetServerAPI(string api)
    {
        return string.Format("{0}{1}", server_addr, api);
    }

    IEnumerator BeginConnectToServer()
    {
        myInfo.name = nameInputField.text;
        var www = MakePostRequest("/enter", myInfo);

        while (!www.isDone)
        {
            if (!MessageBox.IsShown())
            {
                MessageBox.Show("접속 중");
            }
            yield return null;
        }

        if (www.error != null)
        {
            MessageBox.Show(string.Format("접속 실패\n{0}", www.error));
            yield break;
        }

        MessageBox.Hide();

        try
        {
            ParseServerEvents(www.text);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

        Invoke("Poll", 3);
        //        var welcomeEvent = JsonUtility.FromJson<WelcomeEvent>(www.text);

    }

    IEnumerator BeginSendPlayRequest()
    {
        var www = MakePostRequest(string.Format("/play?session={0}", session), null);

        matchSearchPopup.gameObject.SetActive(true);

        yield return www;

        if (www.error != null)
        {
            MessageBox.Show(string.Format("플레이 오류\n{0}", www.error));
            matchSearchPopup.gameObject.SetActive(false);
            yield break;
        }

        ParseServerEvents(www.text);
    }

    IEnumerator BeginSendStayRequest()
    {
        matchSearchPopup.gameObject.SetActive(false);

        var www = MakePostRequest(string.Format("/stay?session={0}", session), null);

        yield return www;

        if (www.error != null)
        {
            Debug.Log("stay error : " + www.error);
        }
    }

    IEnumerator BeginPollRequest()
    {
        var www = MakeGetRequest(string.Format("/poll?session={0}", session));

        yield return www;

        if (www.error != null)
        {
            Debug.Log("poll error : " + www.error);
            pollFailCount++;
        }
        else
        {
            pollFailCount = 0;
        }

        ParseServerEvents(www.text);

        Invoke("Poll", 3 + 3 * pollFailCount);
    }

    void Poll()
    {
        StartCoroutine(BeginPollRequest());
    }

    WWW MakePostRequest(string api, object jsonData)
    {
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");

        var json = jsonData == null ? "null" : JsonMapper.ToJson(jsonData);
        return new WWW(GetServerAPI(api), System.Text.Encoding.UTF8.GetBytes(json.ToCharArray()), headers);
    }

    WWW MakeGetRequest(string api)
    {
        return new WWW(GetServerAPI(api));
    }

    void ParseServerEvents(string responseBody)
    {
        var reader = new JsonReader(responseBody);
        JsonData data = JsonMapper.ToObject(reader);
        for (int i = 0; i < data.Count; i++)
        {
            JsonData elem = data[i];
            Debug.Log(elem.ToJson());
            switch (elem["type"].ToString())
            {
                case "welcome":
                    OnServerWelcomeEvent(JsonMapper.ToObject<WelcomeEvent>(elem.ToJson()));
                    break;

                case "lobby_list":
                    OnServerLobbyListEvent(JsonMapper.ToObject<LobbyListEvent>(elem.ToJson()));
                    break;

                case "match_start":
                    OnMatchStartEvent(JsonMapper.ToObject<MatchStartEvent>(elem.ToJson()));
                    break;

                case "time_to_declare":
                    OnTimeToDeclareEvent(JsonMapper.ToObject<TimeToDeclareEvent>(elem.ToJson()));
                    break;

                case "foe_declare":
                    OnFoeDeclareEvent(JsonMapper.ToObject<FoeDeclareEvent>(elem.ToJson()));
                    break;

                case "time_to_choose":
                    OnTimeToChooseEvent(JsonMapper.ToObject<TimeToChooseEvent>(elem.ToJson()));
                    break;

                case "match_result":
                    OnMatchResultEvent(JsonMapper.ToObject<MatchResultEvent>(elem.ToJson()));
                    break;

                case "timer_set":
                    OnTimerSetEvent(JsonMapper.ToObject<TimerSetEvent>(elem.ToJson()));
                    break;

                case "game_over":
                    OnGameOverEvent(JsonMapper.ToObject<GameOverEvent>(elem.ToJson()));
                    break;

                case "your_declare":
                    OnYourDeclareEvent(JsonMapper.ToObject<YourDeclareEvent>(elem.ToJson()));
                    break;

                case "your_choice":
                    OnYourChoiceEvent(JsonMapper.ToObject<YourChoiceEvent>(elem.ToJson()));
                    break;

                case "chat":
                    OnChatEvent(JsonMapper.ToObject<ChatEvent>(elem.ToJson()));
                    break;
            }
        }
    }

    void OnServerWelcomeEvent(WelcomeEvent e)
    {
        session = e.session;
        is_admin = e.is_admin;
        if (is_admin)
        {
            adminToolGroup.gameObject.SetActive(true);
        }
        SetStatus((Player.Status)Enum.Parse(typeof(Player.Status), e.status, true));
    }

    void OnServerLobbyListEvent(LobbyListEvent e)
    {
        var playersToDelete = players.Where(x => !e.list.Any(y => y.name == x.name));
        var playersToAdd = e.list.Where(x => players.All(y => y.name != x.name));

        foreach (var item in playersToAdd)
        {
            var p = new Player();
            p.name = item.name;
            p.point = item.point;
            p.reliability = (float)item.reliability;
            p.status = (Player.Status)Enum.Parse(typeof(Player.Status), item.status, true);

            players.Add(p);
            lobbyPlayerList.Add(p);
        }

        foreach (var player in playersToDelete)
        {
            players.Remove(player);
            lobbyPlayerList.Remove(player);
        }
        
        for (int i = 0; i < players.Count; i++)
        {
            var player = players[i];
            var item = e.list.Find(x => x.name == player.name);
            player.point = item.point;
            player.reliability = (float)item.reliability;
            player.status = (Player.Status)Enum.Parse(typeof(Player.Status), item.status, true);
        }

        players.Sort(RankComparison);

        int sameRankCount = 1;
        for (int i = 0; i < players.Count; i++)
        {
            var player = players[i];
            if (i > 0 && players[i - 1].point == player.point)
            {
                sameRankCount++;
            }
            else
            {
                sameRankCount = 0;
            }
            player.rank = 1 + i - sameRankCount;

            
            player.NotifyChanged();
        }

        lobbyPlayerList.ReSortByRank();
    }

    int RankComparison(Player lhs, Player rhs)
    {
        return rhs.point - lhs.point;
    }

    void OnMatchStartEvent(MatchStartEvent e)
    {
        matchSearchPopup.gameObject.SetActive(false);
        template = matchTemplates[e.template_id];

        myProfile.Apply(me);

        if (!players.Exists(x => x.name == e.foe.name))
        {
            foe = new Player();
            foe.name = e.foe.name;
            foe.point = e.foe.point;
            foe.reliability = (float)e.foe.reliability;
            foe.status = Player.Status.Matched;
            players.Add(foe);
            lobbyPlayerList.Add(foe);
        }
        foe = players.Find(x => x.name == e.foe.name);
        foeProfile.Apply(foe);

        declareButton.gameObject.SetActive(false);
        chooseButton.gameObject.SetActive(false);

        passButton.gameObject.SetActive(false);

        qq.sprite = qqSprite;

        foeDeclare = foeChoice = myDeclare = myChoice = null;

        isChooseTime = false;

        mainBoard.ShowTable(template);

        myDeclareTxt.Hide();

        restTimeLabel.gameObject.SetActive(false);
        restTimeTxt.gameObject.SetActive(false);

        SetStatus(Player.Status.Matched);
    }

    void OnTimeToDeclareEvent(TimeToDeclareEvent e)
    {
        declareButton.gameObject.SetActive(true);
        declareButton.interactable = false;

        confessButton.gameObject.SetActive(true);
        silentButton.gameObject.SetActive(true);
        passButton.gameObject.SetActive(true);
        passButton.interactable = e.pass_available;

        restTimeLabel.gameObject.SetActive(true);
        restTimeTxt.gameObject.SetActive(true);
        restTimeTxt.Set(Mathf.FloorToInt((float)e.rest_time));

        if (e.pass_available)
        {
            // 두 번쨰
            TwoLineMessageBox.Show(GetFoeDeclareTxt(), "내가 선언할 차례");
        }
        else
        {
            // 첫 번째
            TwoLineMessageBox.Show("", "내가 선언할 차례");
        }
    }

    string GetFoeDeclareTxt()
    {
        if (foeDeclare == "silent")
        {
            return "상대는 침묵을 선언";
        }
        else if (foeDeclare == "confess")
        {
            return "상대는 자백을 선언";
        }
        else
        {
            return "상대는 선언을 거부";
        }
    }

    void OnFoeDeclareEvent(FoeDeclareEvent e)
    {
        foeDeclare = e.what;
        foeDeclareBox.Apply(foeDeclare);
    }

    void OnTimeToChooseEvent(TimeToChooseEvent e)
    {
        isChooseTime = true;

        // passButton.gameObject.SetActive(false);

        declareButton.gameObject.SetActive(false);
        chooseButton.gameObject.SetActive(true);
        chooseButton.interactable = false;

        restTimeLabel.gameObject.SetActive(true);
        restTimeTxt.gameObject.SetActive(true);
        restTimeTxt.Set(Mathf.FloorToInt((float)e.rest_time));
        TwoLineMessageBox.Show(GetFoeDeclareTxt(), "서로 선택할 차례");
    }

    void OnMatchResultEvent(MatchResultEvent e)
    {
        foeChoice = e.foe_choice;
        matchResultBox.onDisabled += OnMatchResultDisabled;
        matchResultBox.gameObject.SetActive(true);
        matchResultBox.Apply(me, foe, myDeclare, foeDeclare, myChoice, foeChoice, template);
    }

    void OnTimerSetEvent(TimerSetEvent e)
    {
        gameOverTimer.Set(e.time);
    }

    void OnGameOverEvent(GameOverEvent e)
    {
        MessageBox.Show("게임 종료");
        SetStatus(Player.Status.Lobby);
        gameOverTimer.gameObject.SetActive(false);
        adminToolGroup.gameObject.SetActive(false);
        playBtn.gameObject.SetActive(false);
        StopCoroutine("BeginPollRequest");
    }

    void OnYourDeclareEvent(YourDeclareEvent e)
    {
        myDeclare = e.your_declare;

        myDeclareTxt.Apply(myDeclare);

        declareButton.gameObject.SetActive(false);
    }

    void OnYourChoiceEvent(YourChoiceEvent e)
    {
        myChoice = e.your_choice;

        chooseButton.gameObject.SetActive(false);
    }

    void OnChatEvent(ChatEvent e)
    {
        var player = players.Find(x => x.name == e.name);

        if (player == null)
        {
            Debug.Log("Unidentified player : " + e.name + " with msg : " + e.msg);
            return;
        }

        chatInputField.OnChatMessage(player, e.msg);
    }

    void OnMatchResultDisabled()
    {
        matchResultBox.onDisabled -= OnMatchResultDisabled;
        SetStatus(Player.Status.Lobby);
    }

    void SetStatus(Player.Status status)
    {
//        this.status = status;

        switch (status)
        {
            case Player.Status.Lobby:
                titleScene.gameObject.SetActive(false);
                matchScene.gameObject.SetActive(false);
                lobbyScene.gameObject.SetActive(true);
                break;

            case Player.Status.Matched:
                titleScene.gameObject.SetActive(false);
                matchScene.gameObject.SetActive(true);
                lobbyScene.gameObject.SetActive(false);
                break;
        }
    }

    IEnumerator BeginDeclare(string what)
    {
        myDeclare = what;

        var www = MakePostRequest(string.Format("/declare?what={0}&session={1}", what, session), null);

        yield return www;

        if (www.error != null)
        {
            MessageBox.Show(string.Format("오류\n{0}", www.error));
        }
    }

    IEnumerator BeginChoose(string what)
    {
        myChoice = what;

        var www = MakePostRequest(string.Format("/choose?what={0}&session={1}", what, session), null);

        yield return www;

        if (www.error != null)
        {
            MessageBox.Show(string.Format("오류\n{0}", www.error));
        }
    }

    IEnumerator BeginSetGameOverTimer()
    {
        int timeInSecs = int.Parse(gameOverTimeInputField.text);

        var www = MakePostRequest(string.Format("/set_timer?time={0}&session={1}", timeInSecs, session), null);

        yield return www;

        if (www.error != null)
        {
            MessageBox.Show(string.Format("오류\n{0}", www.error));
        }
    }

    public Player me
    {
        get
        {
            return players.Find(x => x.name == myInfo.name);
        }
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        titleScene.gameObject.SetActive(true);
    }

    public void Chat(string msg)
    {
        StartCoroutine(BeginChat(msg));    
    }

    IEnumerator BeginChat(string msg)
    {
        var body = new ChatBody();
        body.msg = msg;
        var post = MakePostRequest(string.Format("/chat?session={0}", session), body);

        yield return post;

        if (post.error != null)
        {
            Debug.Log("chat error : " + post.error);
        }
    }

    class ChatBody
    {
        public string msg;
    }
}

public class MyInfo
{
    public string name;
}