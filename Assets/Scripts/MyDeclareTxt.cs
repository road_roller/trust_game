﻿using UnityEngine;
using System.Collections;

public class MyDeclareTxt : MonoBehaviour
{
    public RectTransform confessSock;
    public RectTransform silentSock;
    public RectTransform passSock;

    RectTransform RT;

    void Awake()
    {
        RT = GetComponent<RectTransform>();
    }

    public void Apply(string myDeclare)
    {
        gameObject.SetActive(true);

        if (myDeclare == "confess")
        {
            RT.SetParent(confessSock, false);
        }
        else if (myDeclare == "silent")
        {
            RT.SetParent(silentSock, false);
        }
        else
        {
            RT.SetParent(passSock, false);
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
