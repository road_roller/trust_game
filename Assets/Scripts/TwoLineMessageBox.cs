﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TwoLineMessageBox : MonoBehaviour
{
    public Text topTxt;
    public Text bottomTxt;
    public GameObject obj;

    static TwoLineMessageBox instance;

    public static void Show(string top, string bottom)
    {
        instance._Show(top, bottom);
    }

    public static void Hide()
    {
        instance._Hide();
    }

    public static bool IsShown()
    {
        return instance.obj.activeSelf;
    }

    public void _Show(string top, string bottom)
    {
        topTxt.text = top;
        bottomTxt.text = bottom;
        obj.SetActive(true);
    }

    public void _Hide()
    {
        obj.SetActive(false);
    }

    void Awake()
    {
        instance = this;
    }

    void OnDestroy()
    {
        instance = null;
    }
}
