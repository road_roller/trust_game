﻿using UnityEngine;
using System.Collections;

public class MatchTemplate
{
    public int all_silent;
    public int all_confess;
    public int one_silent;
    public int one_confess;

    public MatchTemplate(int all_silent
        , int all_confess
        , int one_silent
        , int one_confess)
    {
        this.all_silent = all_silent;
        this.all_confess = all_confess;
        this.one_silent = one_silent;
        this.one_confess = one_confess;
    }

    public int GetPoint(string myChoice, string foeChoice)
    {
        if (myChoice == "silent" && foeChoice == "silent")
        {
            return all_silent;
        }
        else if (myChoice == "confess" && foeChoice == "confess")
        {
            return all_confess;
        }
        else
        {
            if (myChoice == "confess" && foeChoice == "silent")
            {
                return one_confess;
            }
            else if (myChoice == "silent" && foeChoice == "confess")
            {
                return one_silent;
            }
            else
            {
                throw new System.Exception(string.Format("unhandled choice {0} {1}", myChoice, foeChoice));
            }
        }
    }
}
