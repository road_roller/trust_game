﻿using UnityEngine;
using System.Collections;

public class PlayerColors
{
    static Color [] colorTable = {
        new Color(168,8,8), new Color(8, 52, 152), new Color(32, 144, 112),
        new Color(250, 250, 124), new Color(0, 127, 127), new Color(200, 162, 144),
        new Color(64, 64, 64), new Color(213, 197, 6), new Color(230, 230, 230), new Color(20, 20, 20)
    };

    static int idx = 0;

    static PlayerColors()
    {
        for (int i = 0; i < colorTable.Length; i++)
        {
            colorTable[i].r /= 255.0f;
            colorTable[i].g /= 255.0f;
            colorTable[i].b /= 255.0f;
        }
    }

    public static Color GetColor()
    {
        var c = colorTable[idx];
        idx = (idx + 1) % colorTable.Length;
        return c;
    }

}
