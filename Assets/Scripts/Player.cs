﻿using UnityEngine;
using System.Collections;

public class Player
{
    public enum Status
    {
        Lobby
        , Matched
    }

    public string name;
    public int point;
    public float reliability;
    public Status status = Status.Lobby;
    public int rank;
    public Color color;

    public event OnChanged onChanged;
    public delegate void OnChanged();

    public Player()
    {
        color = PlayerColors.GetColor();
    }

    public void NotifyChanged()
    {
        if (onChanged != null)
        {
            onChanged();
        }
    }
}
