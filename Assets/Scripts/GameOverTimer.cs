﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class GameOverTimer : MonoBehaviour
{
    Text txt;

    DateTime overTime;
    bool clocking = false;
    int cachedN;

    void Awake()
    {
        txt = GetComponent<Text>();
    }

    public void Set(int restTimeInSec)
    {
        overTime = DateTime.Now.AddSeconds(restTimeInSec);
        clocking = true;
    }

    void Update()
    {
        if (!clocking)
        {
            return;
        }

        int restTimeInSec = (int)((overTime - DateTime.Now).TotalSeconds);
        if (restTimeInSec < 0)
        {
            return;
        }

        if (restTimeInSec != cachedN)
        {
            if (restTimeInSec < 60)
            {
                txt.text = restTimeInSec.ToString();
            }
            else
            {
                txt.text = string.Format("{0}:{1}"
                    , restTimeInSec / 60, restTimeInSec % 60);
            }
            cachedN = restTimeInSec;
        }
    }
}
