﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class LobbyPlayerList : MonoBehaviour
{
    public RectTransform content;
    public LobbyUserItem lobbyUserItemPrefab;
    public float PADDING;

    struct ManageItem
    {
        public Player player;
        public LobbyUserItem userItem;
    }

    List<ManageItem> items = new List<ManageItem>();

    public void Clear()
    {
        foreach (var child in content)
        {
            var childT = (Transform)child;
            Destroy(childT.gameObject);
        }
    }

    public void Add(Player player)
    {
        var lobbyUserItem = Instantiate(lobbyUserItemPrefab);
        lobbyUserItem.Apply(player);
        lobbyUserItem.transform.SetParent(content, false);
        var mItem = new ManageItem();
        mItem.player = player;
        mItem.userItem = lobbyUserItem;
        items.Add(mItem);

        ResizeContent();
    }

    public void Remove(Player player)
    {
        foreach (var mItem in items)
        {
            if (mItem.player.name == player.name)
            {
                Destroy(mItem.userItem);
                items.Remove(mItem);
                break;
            }
        }

        ResizeContent();
    }

    void ResizeContent()
    {
        var spacing = content.GetComponent<VerticalLayoutGroup>().spacing;
        float sum = items.Sum(x => spacing + x.userItem.GetComponent<RectTransform>().rect.height);
//        var RT = content.rect;
        sum += PADDING;
        content.sizeDelta = new Vector2(0, sum);
//        content.rect.Set(RT.x, RT.y, RT.width, sum);

    }

    public void ReSortByRank()
    {
        for (int i = 0; i < items.Count; i++)
        {
            int k = i;
            for (int j = i + 1; j < items.Count; j++)
            {
                if (items[k].player.point < items[j].player.point)
                {
                    k = j;
                }
            }

            var coke = items[k];
            items[k] = items[i];
            items[i] = coke;

            items[i].userItem.transform.SetSiblingIndex(i);
        }
    }
}
